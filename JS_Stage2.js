//Define vars
var site = window.location.hostname;
var classPrice = (site.indexOf('iberia')!=-1)?'total-price bold':'amount';
var classDate = (site.indexOf('iberia')!=-1)?'date':'bound_date';
var classInfo = (site.indexOf('iberia')!=-1)?'clearfix rbDispo':'bound_details';
var classCity = (site.indexOf('iberia')!=-1)?'city':'t1__od__airport';
var classAirport = (site.indexOf('iberia')!=-1)?'airport':'t1__od__airport';
var classTerminal = (site.indexOf('iberia')!=-1)?'terminal-info':'';
var classTime = (site.indexOf('iberia')!=-1)?'hour':'t1__od__date t1__picto__disk--grey';
var classSegment = (site.indexOf('iberia')!=-1)?'scale':'t1__od__segment';
var depart = (site.indexOf('iberia')!=-1)?'depart':'t1__od__ori';
var dest = (site.indexOf('iberia')!=-1)?'arrival':'t1__od__dest';

//Format functions
function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'').replace(/\n/g,' ');
}

//Create response
function Response(flight) {
	this.flight = flight;
}

//Define response structure:
function Flight(details) {
	this.price = myTrim(document.getElementsByClassName(classPrice)[0].textContent);
	this.details = details;
}

//Generate JSON content
function Route(pos1, pos2, origin, destination) {
	this.number = (site.indexOf('iberia')!=-1)?(document.getElementsByClassName('clearfix rbDispo')[pos1].getElementsByClassName('flight-code')[pos2].textContent):'';
	this.company = (site.indexOf('iberia')!=-1)?myTrim(document.getElementsByClassName('clearfix rbDispo')[pos1].getElementsByClassName('company')[pos2].textContent):'';
	this.date = document.getElementsByClassName(classDate)[pos1].textContent;
	this.origin = origin;
	this.destination = destination;
}


function Info(className, pos1, pos2) {
	if (site.indexOf('iberia')!=-1) {
		this.city = document.getElementsByClassName(classInfo)[pos1].getElementsByClassName(className)[pos2].getElementsByClassName(classCity)[0].textContent;
		this.airport = document.getElementsByClassName(classInfo)[pos1].getElementsByClassName(className)[pos2].getElementsByClassName(classAirport)[0].textContent;
		this.terminal = myTrim(document.getElementsByClassName(classInfo)[pos1].getElementsByClassName(className)[pos2].getElementsByClassName(classTerminal)[0].textContent);
	} else if (site.indexOf('airfrance')!=-1) {
		var n = document.getElementsByClassName('bound_details')[pos1].getElementsByClassName(className)[pos2].getElementsByClassName('t1__od__airport')[0].textContent.indexOf(',');
		this.city = (document.getElementsByClassName(classInfo)[pos1].getElementsByClassName(className)[pos2].getElementsByClassName(classCity)[0].textContent).substring(0,n);
		this.airport = (document.getElementsByClassName(classInfo)[pos1].getElementsByClassName(className)[pos2].getElementsByClassName(classAirport)[0].textContent).substring(n+2);
		this.terminal = classTerminal;
	}
	this.time = myTrim(document.getElementsByClassName(classInfo)[pos1].getElementsByClassName(className)[pos2].getElementsByClassName(classTime)[0].textContent);
}

var details = new Array();


for (j = 0; j < document.getElementsByClassName(classInfo).length; j++) { 
	for (i = 0; i < document.getElementsByClassName(classInfo)[j].getElementsByClassName(classSegment).length; i++) { 
		details.push(new Route(j, i, new Info(depart, j, i), new Info(dest, j, i)));
	}
}

var flight = new Flight(details);
var response = new Response(flight);

//Build the JSON response
//Print on console
console.log(JSON.stringify(response));

//Send to api.capturedata.ie
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
	   if (this.readyState == 4) {
		   if (this.status == 200) {
			   alert('Process completed successfully');
		   } else if (this.status == 403) {
			   alert('Forbidden');
		   } else if (this.status == 404) {
			   alert('Page not found');
		   } else {
			   alert('Other error');
		   }
	   }
    };
xhttp.open("POST", "api.capturedata.ie", true);
xhttp.setRequestHeader("Content-type", "text/plain");
xhttp.send(JSON.stringify(response));