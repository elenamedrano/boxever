//Format functions
function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'').replace(/\n/g,' ');
}

//Define response
function Response(flight) {
	this.flight = flight;
}

//Define response structure:
function Flight(details) {
	this.price = myTrim(document.getElementsByClassName('total-price bold')[0].textContent);
	this.details = details;
}

//Generate JSON content
function Route(pos1, pos2, origin, destination) {
	this.number = document.getElementsByClassName('clearfix rbDispo')[pos1].getElementsByClassName('flight-code')[pos2].textContent;
	this.company = myTrim(document.getElementsByClassName('clearfix rbDispo')[pos1].getElementsByClassName('company')[pos2].textContent);
	this.date = document.getElementsByClassName('date')[pos1].textContent;
	this.origin = origin;
	this.destination = destination;
}

function Info(className, pos1, pos2) {
	this.city = document.getElementsByClassName('clearfix rbDispo')[pos1].getElementsByClassName(className)[pos2].getElementsByClassName("city")[0].textContent;
	this.airport = document.getElementsByClassName('clearfix rbDispo')[pos1].getElementsByClassName(className)[pos2].getElementsByClassName("airport")[0].textContent;
	this.terminal = myTrim(document.getElementsByClassName('clearfix rbDispo')[pos1].getElementsByClassName(className)[pos2].getElementsByClassName("terminal-info")[0].textContent);
	this.time = myTrim(document.getElementsByClassName('clearfix rbDispo')[pos1].getElementsByClassName(className)[pos2].getElementsByClassName("hour")[0].textContent);
}

var details = new Array();



for (j = 0; j < document.getElementsByClassName('clearfix rbDispo').length; j++) { 
	for (i = 0; i < document.getElementsByClassName('clearfix rbDispo')[j].getElementsByClassName('scale').length; i++) { 
		details.push(new Route(j, i, new Info('depart',j, i), new Info('arrival',j, i)));
	}
}

var flight = new Flight(details);
var response = new Response(flight);

//Build the response JSON
//Print on console
console.log(JSON.stringify(response));

//Send to api.capturedata.ie
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
	   if (this.readyState == 4) {
		   if (this.status == 200) {
			   alert('Process completed successfully');
		   } else if (this.status == 403) {
			   alert('Forbidden');
		   } else if (this.status == 404) {
			   alert('Page not found');
		   } else {
			   alert('Other error');
		   }
	   }
    };
xhttp.open("POST", "api.capturedata.ie", true);
xhttp.setRequestHeader("Content-type", "text/plain");
xhttp.send(JSON.stringify(response));